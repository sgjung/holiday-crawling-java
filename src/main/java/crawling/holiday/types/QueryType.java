package crawling.holiday.types;

import java.util.HashMap;
import java.util.Map;

public enum QueryType {
    PUBLIC(1, "getHoliDeInfo"){
        @Override
        public String getKorMenu(){
            return "국경일 정보조회";
        }
    },
    REST(2, "getRestDeInfo"){
        @Override
        public String getKorMenu(){
            return "뭔지 모름 쓰잘데 없음";
        }
    },
    DIVISION(3, "get24DivisionsInfo"){
        @Override
        public String getKorMenu(){
            return "24절기 조회";
        }
    },
    SUNDRY(4, "getSundryDayInfo"){
        @Override
        public String getKorMenu(){
            return "잡절 조회";
        }
    };

    private int queryId;
    private String queryCd;
    private static Map map = new HashMap();
    private final String API_HOLIDAY_BASE_URL = "http://apis.data.go.kr/B090041/openapi/service/SpcdeInfoService";

    public abstract String getKorMenu();

    QueryType(int queryId, String queryCd){
        this.queryId = queryId;
        this.queryCd = queryCd;
    }

    static{
        for(QueryType queryType : QueryType.values()){
            map.put(queryType.queryId, queryType);
        }
    }

    public static QueryType valueOf(int queryId){
        return (QueryType) map.get(queryId);
    }

    public int getQueryId() {
        return queryId;
    }

    public void setQueryId(int queryId) {
        this.queryId = queryId;
    }

    public String getQueryCd() {
        return queryCd;
    }

    public void setQueryCd(String queryCd) {
        this.queryCd = queryCd;
    }

    public String getApiUrl(){
        return API_HOLIDAY_BASE_URL;
    }

}
