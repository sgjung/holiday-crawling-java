package crawling.holiday;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import crawling.APIType;
import crawling.holiday.types.QueryType;
import org.w3c.dom.CharacterData;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.StringReader;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HolidayCrawling {
    public static void main(String [] args){
        HolidayCrawling h = new HolidayCrawling();
        String apiResult = h.getPublicHolidayList("2019", "05");
        System.out.println("============ apiResult ============");
        System.out.println(apiResult);
        System.out.println("====================================");
        List<Map<String,Object>> resultList = h.getParsedXMLResult(apiResult);
    }

    private static String getElementValue(Element e) {
        Node child = e.getFirstChild();
        if(child instanceof CharacterData) {
            CharacterData data = (CharacterData) child;
            return data.getData();
        }
        return "-";
    }

    // 1) API 리퀘스트
    public String getPublicHolidayList(String year, String month){
        String apiResult = "";
        String reqUrl = APIType.HOLIDAYS_AND_24DIVISIONS.getAPIUrl() + "/" + QueryType.PUBLIC.getQueryCd()
                            + "?" + "solYear=" + year + "&" + "solMonth=" + month + "&"
                            + "ServiceKey=" + APIType.HOLIDAYS_AND_24DIVISIONS.getAPIKey();
//        http://apis.data.go.kr/B090041/openapi/service/SpcdeInfoService/getHoliDeInfo?solYear=2019&solMonth=05&ServiceKey=

        System.out.println("reqUrl :: " + reqUrl);
        Client client = Client.create();

        WebResource webResource = client.resource(reqUrl);
        ClientResponse response = webResource.accept("application/xml")
                .get(ClientResponse.class);

        if(response.getStatus() != 200) {
            throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
        }

        apiResult = response.getEntity(String.class);
        return apiResult;
    }

    // 2) XML DOM 빌드 -> List<Map<String,Object>> 변환
    public List<Map<String,Object>> getParsedXMLResult(String apiResult){
        List<Map<String,Object>> resultList = new ArrayList<Map<String, Object>>();

        DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder;
        try {
            builder = builderFactory.newDocumentBuilder();
            InputSource inputSource = new InputSource();
            inputSource.setCharacterStream(new StringReader(apiResult));
            Document domParsed = builder.parse(inputSource);
            NodeList itemList = domParsed.getElementsByTagName("item");

            for(int i=0; i<itemList.getLength(); i++) {
                Element element = (Element) itemList.item(i);
                Element dateKind = (Element) element.getElementsByTagName("dateKind").item(0);
                Element dateName = (Element) element.getElementsByTagName("dateName").item(0);
                Element isHoliday = (Element) element.getElementsByTagName("isHoliday").item(0);
                Element locdate = (Element) element.getElementsByTagName("locdate").item(0);
                Element seq = (Element) element.getElementsByTagName("seq").item(0);

                Map<String, Object> holiday = new HashMap<>();
                holiday.put("dateKind", getElementValue(dateKind));
                holiday.put("dateName", getElementValue(dateName));
                holiday.put("isHoliday", getElementValue(isHoliday));
                holiday.put("locdate", getElementValue(locdate));
                holiday.put("seq", getElementValue(seq));

                String day = "";
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");
                LocalDate date = LocalDate.parse(holiday.get("locdate").toString(), formatter);

                int dayNum = date.getDayOfWeek().getValue();
                switch(dayNum) {
                    case 1:
                        day = "월요일";
                        break;
                    case 2:
                        day = "화요일";
                        break;
                    case 3:
                        day = "수요일";
                        break;
                    case 4:
                        day = "목요일";
                        break;
                    case 5:
                        day = "금요일";
                        break;
                    case 6:
                        day = "토요일";
                        break;
                    case 7:
                        day = "일요일";
                        break;
                }

                holiday.put("DAY_CD", String.valueOf(dayNum));
                holiday.put("DAY_NM", day);
                holiday.put("D_REG_DT", LocalDate.now().format(formatter));
                holiday.put("D_UPDATE_DT", LocalDate.now().format(formatter));
                holiday.put("BIGO", holiday.get("dateName").toString());
                holiday.put("V_TIME", holiday.get("locdate").toString());

                resultList.add(holiday);
                System.out.println(holiday.toString());
            }
//            System.out.println(resultList.toString());

        } catch (ParserConfigurationException | SAXException | IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return resultList;
    }
}
